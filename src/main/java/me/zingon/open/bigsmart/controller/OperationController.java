package me.zingon.open.bigsmart.controller;

import me.zingon.open.bigsmart.model.Job;
import me.zingon.open.bigsmart.service.OperationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author ztc 1423047407@qq.com
 * @version 1.0
 * @date 2020-8-6 14:07
 */
@RestController
@RequestMapping("/o")
public class OperationController {

    @Autowired
    OperationService operationService;

    @PostMapping("/add")
    public Object add(@RequestBody Job job){
        return operationService.addJob(job);
    }

    @GetMapping("/get")
    public Object get(String topic){
        return operationService.getReadyJob(topic);
    }

    @GetMapping("/complete")
    public Object rm(String topic,String id){
        return operationService.rmComplete(topic,id);
    }

}
