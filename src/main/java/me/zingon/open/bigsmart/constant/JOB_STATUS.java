package me.zingon.open.bigsmart.constant;

/**
 * @author ztc 1423047407@qq.com
 * @version 1.0
 * @date 2020-8-6 15:41
 */
public enum JOB_STATUS {
    /**
     * ready：可执行状态，等待消费。
     */
    READY(1),

    /**
     * delay：不可执行状态，等待时钟周期。
     */
    DEALY(2),

    /**
     * reserved：已被消费者读取，但还未得到消费者的响应（delete、finish）。
     */
    RESERVED(3),

    /**
     * deleted：已被消费完成或者已被删除。
     */
    DELETED(4);

    Integer code;

    JOB_STATUS(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }
}
