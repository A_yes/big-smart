package me.zingon.open.bigsmart.runner;

import lombok.extern.slf4j.Slf4j;
import me.zingon.open.bigsmart.queue.WorkerList;
import me.zingon.open.bigsmart.thread.TopicTimer;
import me.zingon.open.bigsmart.util.ApplicationContextHolder;
import me.zingon.open.bigsmart.worker.IWorker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * @author ztc 1423047407@qq.com
 * @version 1.0
 * @date 2020-8-6 16:36
 */
@Component
@Slf4j
public class WorkerRunner implements ApplicationRunner {

    @Autowired
    TopicTimer topicTimer;

    @Autowired
    IWorker httpTopicWorker;

    @Autowired
    ApplicationContextHolder applicationContextHolder;

    @Autowired
    WorkerList workerList;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        workerList.getList().stream().forEach(w->{
            topicTimer.run(w.get("topic"), (IWorker) applicationContextHolder.getBeanByName(w.get("worker")));
            log.info("runnner:{}",w);
        });
    }

}
