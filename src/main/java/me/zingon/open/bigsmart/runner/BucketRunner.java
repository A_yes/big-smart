package me.zingon.open.bigsmart.runner;

import lombok.extern.slf4j.Slf4j;
import me.zingon.open.bigsmart.queue.DelayJobBucket;
import me.zingon.open.bigsmart.thread.BucketTimer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.Set;

/**
 * @author ztc 1423047407@qq.com
 * @version 1.0
 * @date 2020-8-12 09:53
 */
@Slf4j
@Component
public class BucketRunner implements ApplicationRunner {

    @Autowired
    DelayJobBucket delayJobBucket;

    @Autowired
    BucketTimer bucketTimer;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        Set<String> topic = delayJobBucket.listTopic();
        topic.stream().forEach(t->bucketTimer.start(t));
    }
}
