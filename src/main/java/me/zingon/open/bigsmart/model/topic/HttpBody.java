package me.zingon.open.bigsmart.model.topic;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;

/**
 * @author ztc 1423047407@qq.com
 * @version 1.0
 * @date 2020-8-7 13:43
 */
@Data
@Slf4j
public class HttpBody {

    String url;

    String method;

    Map<String,String> headers;

    String body;

    String passrule;

    public HttpBody() {
    }

    public HttpBody(String body) {
        log.info("转化body：{}",body);
        JSONObject o = JSONUtil.parseObj(body);
        this.url = o.getStr("url");
        this.method = o.getStr("method") == null ? "post": o.getStr("method");
        JSONObject hs = o.getJSONObject("headers");
        this.headers=new HashMap<>();
        hs.forEach((k,v)->{
            this.headers.put(k,v.toString());
        });
        this.body = o.getStr("body");
        this.passrule = o.getStr("passrule");
    }
}
