package me.zingon.open.bigsmart.model;

import lombok.Data;

import java.util.Date;

/**
 * @author ztc 1423047407@qq.com
 * @version 1.0
 * @date 2020-8-6 09:35
 */
@Data
public class Job {

    String id;

    /**
     * 主题
     */
    String topic;

    /**
     * 延迟时间(ms)
     */
    Long delay;

    /**
     * 超时时间，
     * 当任务被消费者取走，此任务会以此时间重新入队
     * 在此时间内服务受到消费者的完成确认会删除此任务，否则重试执行
     */
    Long ttr;

    /**
     * 业务数据
     */
    String body;

    /**
     * 1 ready：可执行状态，等待消费。
     * 2 delay：不可执行状态，等待时钟周期。
     * 3 reserved：已被消费者读取，但还未得到消费者的响应（delete、finish）。
     * 4 deleted：已被消费完成或者已被删除。
     */
    Integer status;

    /**
     * 执行时间（ms）
     */
    Long execTime;

    /**
     * 创建时间(ms)
     */
    Long createTime;

}
