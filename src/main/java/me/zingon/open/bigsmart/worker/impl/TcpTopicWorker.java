package me.zingon.open.bigsmart.worker.impl;

import lombok.extern.slf4j.Slf4j;
import me.zingon.open.bigsmart.model.Job;
import me.zingon.open.bigsmart.queue.JobPoll;
import me.zingon.open.bigsmart.worker.IWorker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author ztc 1423047407@qq.com
 * @version 1.0
 * @date 2020-8-7 09:12
 */
@Component
@Slf4j
public class TcpTopicWorker implements IWorker {

    @Autowired
    JobPoll jobPoll;

    @Override
    public void work(Job job) {
        log.info("tcp消费：{}",job);
        jobPoll.rm(job.getTopic(),job.getId());
    }
}
