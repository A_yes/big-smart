package me.zingon.open.bigsmart.worker;

import me.zingon.open.bigsmart.model.Job;
import org.springframework.scheduling.annotation.Async;

/**
 * @author ztc 1423047407@qq.com
 * @version 1.0
 * @date 2020-8-6 16:15
 */
public interface IWorker {


    /**
     * TODO: 2020-8-7 返回Future判断job消费是否成功，成功需要jobpoll中删除
     */
    @Async("workerTimerExecutor")
    void work(Job job);

}
