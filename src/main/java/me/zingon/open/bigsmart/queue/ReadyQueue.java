package me.zingon.open.bigsmart.queue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Set;

/**
 * @author ztc 1423047407@qq.com
 * @version 1.0
 * @date 2020-8-6 13:33
 */
@Component
public class ReadyQueue {

    @Autowired
    RedisTemplate redisTemplate;

    @Value("${redis.prefix.readyQueue}")
    String PREFIX;

    ListOperations listOperations;

    @PostConstruct
    public void init(){
        listOperations = redisTemplate.opsForList();
    }

    public Long add(String topic,String id){
        String key=PREFIX+topic;
        return listOperations.leftPush(key,id);
    }

    public Long add(String topic, Set<String> ids){
        String key=PREFIX+topic;
        return listOperations.leftPushAll(key,ids);
    }

    public String pop(String topic){
        String key=PREFIX+topic;
        return (String) listOperations.rightPop(key);
    }

}
