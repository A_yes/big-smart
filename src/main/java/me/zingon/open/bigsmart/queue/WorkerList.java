package me.zingon.open.bigsmart.queue;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @author ztc 1423047407@qq.com
 * @version 1.0
 * @date 2020-8-7 09:22
 */
@Component
@ConfigurationProperties(prefix = "worker")
@Data
public class WorkerList {
    List<Map<String,String>> list;
}
