package me.zingon.open.bigsmart.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * @author ztc 1423047407@qq.com
 * @version 1.0
 * @date 2020-8-6 17:01
 */
@Component
public class ApplicationContextHolder implements ApplicationContextAware {
    ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext=applicationContext;
    }


    public Object getBeanByName(String name){
        return applicationContext != null ? applicationContext.getBean(name) : null;
    }
}
